package pt.nunocruz.wtest

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import pt.nunocruz.wtest.ex1.CodPostalList
import pt.nunocruz.wtest.ex2.TransparencyBarList
import pt.nunocruz.wtest.ex3.EditTextList
import pt.nunocruz.wtest.ex4.Webview

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Bottom Navigation View onClick
        navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.ex_1 -> createFragment(CodPostalList())
                R.id.ex_2 -> createFragment(TransparencyBarList())
                R.id.ex_3 -> createFragment(EditTextList())
                R.id.ex_4 -> createFragment(Webview())
            }

            return@setOnNavigationItemSelectedListener true
        }

        //Pre-select first item
        navigation.selectedItemId = R.id.ex_1
    }

    fun createFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit()
    }
}
