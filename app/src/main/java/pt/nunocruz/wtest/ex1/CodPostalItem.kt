package pt.nunocruz.wtest.ex1

import com.j256.ormlite.field.DatabaseField

/**
 * Created by Nuno on 17/02/2018.
 */

class CodPostalItem(@DatabaseField private val num_cod_postal : String = "", @DatabaseField private val ext_cod_postal : String = "", @DatabaseField private val nome_localidade : String = "") {
    val displayText
            get() = "$num_cod_postal-$ext_cod_postal, $nome_localidade." //Returns a formatted string "XXXX-YYY, Local"
}