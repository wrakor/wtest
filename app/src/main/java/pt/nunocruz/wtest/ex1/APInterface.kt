package pt.nunocruz.wtest.ex1

import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Nuno on 17/02/2018.
 */
interface APIInterface {

    @GET("codigos_postais.json")
    fun getPostalCodes(): Call<List<CodPostalItem>>
}