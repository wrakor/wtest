package pt.nunocruz.wtest.ex1

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.codpostal_item.view.*
import pt.nunocruz.wtest.R

/**
 * Created by Nuno on 17/02/2018.
 */

class RecyclerAdapter(var items: List<CodPostalItem>, private val context: Context) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(LayoutInflater.from(context).inflate(R.layout.codpostal_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: CodPostalItem) {
           itemView.cod_postal.text = item.displayText
        }
    }
}