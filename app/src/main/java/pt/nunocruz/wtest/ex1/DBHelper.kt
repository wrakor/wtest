package pt.nunocruz.wtest.ex1

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils

/**
 * Created by Nuno on 17/02/2018.
 */
class DBHelper(context: Context, databaseName: String, factory: SQLiteDatabase.CursorFactory?, databaseVersion: Int = 1) : OrmLiteSqliteOpenHelper(context, databaseName, factory, databaseVersion) {

    //Data Access Object for CodPostalItem table
    val codPostalDao = getDao(CodPostalItem::class.java)

    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        TableUtils.createTable<CodPostalItem>(connectionSource, CodPostalItem::class.java)
    }

    override fun onUpgrade(database: SQLiteDatabase?, connectionSource: ConnectionSource?, oldVersion: Int, newVersion: Int) {
        TableUtils.dropTable<CodPostalItem, Any>(connectionSource, CodPostalItem::class.java, true)
    }

    fun findPostalCodesWith(query: String): List<CodPostalItem> {
        /*val queryBuilder = codPostalDao.queryBuilder()
        queryBuilder.where().like("num_cod_postal", "%$query%").or().like("ext_cod_postal", "%$query%").or().like("nome_localidade", "%$query%")*/

        val sqlQuery = "SELECT * FROM CodPostalItem WHERE num_cod_postal LIKE \"%$query%\" OR ext_cod_postal LIKE \"%$query%\" OR nome_localidade LIKE \"%$query%\" COLLATE NOACCENTS"

        return codPostalDao.queryRaw(sqlQuery, codPostalDao.rawRowMapper).results as List<CodPostalItem>
    }
}