package pt.nunocruz.wtest.ex1

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import kotlinx.android.synthetic.main.codpostal_list.*
import kotlinx.android.synthetic.main.codpostal_list.view.*
import okhttp3.OkHttpClient
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk25.coroutines.onEditorAction
import org.jetbrains.anko.uiThread
import pt.nunocruz.wtest.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.Normalizer
import java.util.concurrent.Future

/**
 * Created by Nuno on 17/02/2018.
 */

class CodPostalList : Fragment() {

    var postalCodes = mutableListOf<CodPostalItem>()
    private var searchTasks = mutableListOf<Future<Unit>>()
    private lateinit var dbHelper : DBHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.codpostal_list, container, false)

        dbHelper = DBHelper(context, "WTest.db", null)

        //Query postal codes from database (ormlite)
        postalCodes = dbHelper.codPostalDao.queryForAll()
        val recyclerView = rootView.recycler_view

        //If there are none, fetch them. Otherwise, show them.
        if (postalCodes.isEmpty()) {
            rootView.loading_msg.visibility = VISIBLE
            fetchPostalCodes(rootView, dbHelper)
        } else
            recyclerView.adapter = RecyclerAdapter(postalCodes, activity)

        //Search functionality
        rootView.search_bar.onEditorAction { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val text = v?.text.toString()

                if (text.isEmpty()) {
                    //Cancel current async tasks if there are any
                    searchTasks.forEach { it.cancel(true) }

                    //Update adapter with all items
                    (recyclerView.adapter as RecyclerAdapter).items = postalCodes
                    recyclerView.adapter.notifyDataSetChanged()
                } else
                    filterItems(v?.text.toString(), recyclerView.adapter as RecyclerAdapter)
            }
        }

        return rootView
    }

    private fun filterItems(query: CharSequence, adapter: RecyclerAdapter) {
        loading_icon.visibility = VISIBLE

        //Cancel current async tasks if there are any
        searchTasks.forEach { it.cancel(true) }

        searchTasks.add(doAsync {
            //Filter postal codes
            val filteredList = dbHelper.findPostalCodesWith(query.toString())

            adapter.items = filteredList

            uiThread {
                loading_icon.visibility = GONE

                adapter.notifyDataSetChanged()
            }
        })
    }

    //Removes accents to compare strings in search
    private fun stripAccents(s: String): String {
        var string = s
        string = Normalizer.normalize(string, Normalizer.Form.NFD)
        string = string.replace("[\\p{InCombiningDiacriticalMarks}]".toRegex(), "")
        return string
    }

    private fun fetchPostalCodes(view: View, dbHelper: DBHelper) {
        //Http request using Retrofit. Returns a list of CodPostalItem
        retrofitClient.create(APIInterface::class.java).getPostalCodes().enqueue(object : Callback<List<CodPostalItem>?> {

            override fun onResponse(call: Call<List<CodPostalItem>?>, response: Response<List<CodPostalItem>?>) {
                if (response.isSuccessful) {
                    postalCodes = response.body() as MutableList<CodPostalItem>

                    //Update adapter
                    view.recycler_view.adapter = RecyclerAdapter(postalCodes, activity)
                    view.recycler_view.adapter.notifyDataSetChanged()

                    view.loading_msg.visibility = GONE

                    //Insert items into database
                    dbHelper.codPostalDao.create(postalCodes)
                }
            }

            override fun onFailure(call: Call<List<CodPostalItem>?>?, t: Throwable) {
                t.printStackTrace()
                loading_msg.text = "Failed to fetch postal codes. Do you have an internet connection?"
            }
        })
    }

    private val retrofitClient = Retrofit.Builder()
            .baseUrl("http://centraldedados.pt/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()
}