package pt.nunocruz.wtest.ex4

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.webview.view.*
import pt.nunocruz.wtest.BuildConfig
import pt.nunocruz.wtest.R


/**
 * Created by Nuno on 17/02/2018.
 */

class Webview : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.webview, container, false)

        rootView.webview.webViewClient = WebViewClient()
        rootView.webview.loadUrl(BuildConfig.URL) //Use URL defined in each flavor

        return rootView
    }
}