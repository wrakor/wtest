package pt.nunocruz.wtest.ex2

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.codpostal_item.view.*
import kotlinx.android.synthetic.main.image.view.*
import pt.nunocruz.wtest.R
import pt.nunocruz.wtest.ex1.CodPostalItem

/**
 * Created by Nuno on 17/02/2018.
 */

class RecyclerAdapterWithHeader(private var items: List<CodPostalItem>, private val context: Context) : RecyclerView.Adapter<RecyclerAdapterWithHeader.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = if (viewType == 0) R.layout.image else R.layout.codpostal_item //Layout according to viewtype

        return ViewHolder(LayoutInflater.from(context).inflate(layout, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (getItemViewType(position) == 0)
            holder.bind(context)
        else
            holder.bind(items[position-1])
    }

    override fun getItemCount(): Int = items.size + 1

    override fun getItemViewType(position: Int): Int { //0 is header
        return if (position == 0) 0 else 1
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: CodPostalItem) {
            itemView.cod_postal.text = item.displayText
        }

        fun bind(context: Context) {
            Glide.with(context).load("http://firepitideas.net/site-images/header-photo-3.jpg").into(itemView.image)
        }
    }
}