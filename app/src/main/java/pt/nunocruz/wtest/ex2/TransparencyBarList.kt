package pt.nunocruz.wtest.ex2

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.recycler_view.view.*
import pt.nunocruz.wtest.MainActivity
import pt.nunocruz.wtest.R
import pt.nunocruz.wtest.ex1.CodPostalItem

/**
 * Created by Nuno on 17/02/2018.
 */

class TransparencyBarList : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.recycler_view, container, false)

        //Create dummy list with 50 elements
        val postalCodes = List(50) { CodPostalItem("1234", "321", "Exemplo") }

        //Adapter that automatically puts an image as an header
        rootView.recycler_view.adapter = RecyclerAdapterWithHeader(postalCodes, activity)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val recyclerView = view.recycler_view
        val color = ColorDrawable(resources.getColor(R.color.background_material_dark))
        val supportActionBar = (activity as MainActivity).supportActionBar
        val text = SpannableString(supportActionBar?.title)

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val maxScroll = recyclerView.computeVerticalScrollRange() - recyclerView.height

                val percentage = recyclerView.computeVerticalScrollOffset() / maxScroll.toFloat()

                //When scroll reaches the start or the end, change app bar title color
                if (percentage == 1f) {
                    text.setSpan(ForegroundColorSpan(Color.BLACK), 0, text.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                    supportActionBar?.title = text
                } else if (percentage == 0f) {
                    text.setSpan(ForegroundColorSpan(Color.WHITE), 0, text.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                    supportActionBar?.title = text
                }

                //Change transparency according to scroll
                color.alpha = 255 - (percentage * 255).toInt()
                supportActionBar?.setBackgroundDrawable(color)
            }
        })
    }
}