package pt.nunocruz.wtest.ex3

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.recycler_view.view.*
import pt.nunocruz.wtest.R

/**
 * Created by Nuno on 17/02/2018.
 */

class EditTextList : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.recycler_view, container, false)

        rootView.recycler_view.adapter = RecyclerAdapterEditText(activity)
        return rootView
    }
}