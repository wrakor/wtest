package pt.nunocruz.wtest.ex3

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.ex_3_item.view.*
import pt.nunocruz.wtest.MainActivity
import pt.nunocruz.wtest.R

/**
 * Created by Nuno on 17/02/2018.
 */

class RecyclerAdapterEditText(private val context: Context) : RecyclerView.Adapter<RecyclerAdapterEditText.ViewHolder>() {

    val normalTextPositions = mutableListOf<Int>()
    val numberPositions = mutableListOf<Int>()

    val TEXT = 0
    val NUMBERS = 1
    val CAPS = 2

    init {
        //Separate the 50 elements in 3 groups: text, numbers and caps
        for (i in 0..itemCount step 3)
            normalTextPositions.add(i)

        for (i in 1..itemCount step 3)
            numberPositions.add(i)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(LayoutInflater.from(context).inflate(R.layout.ex_3_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position, getItemGroup(position))
    }

    override fun getItemCount(): Int = 50

    //Disable view recycling to prevent edittext content being replicated
    override fun getItemViewType(position: Int) = position

    fun getItemGroup(position: Int): Int {
        if (normalTextPositions.contains(position))
            return TEXT
        if (numberPositions.contains(position))
            return NUMBERS
        return CAPS
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int, itemType: Int) {

            var text = (position + 1).toString()

            //Change edit text input type according to group
            when (itemType) {
                TEXT -> {
                    itemView.edit_text.inputType = InputType.TYPE_CLASS_TEXT
                    text += " Text"
                }
                NUMBERS -> {
                    itemView.edit_text.inputType = InputType.TYPE_CLASS_NUMBER
                    text += " Number"
                }
                else -> {
                    itemView.edit_text.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
                    text += " Capital Text"
                }
            }

            itemView.text_view.text = text

            //Hide keyboard when touching a textview
            itemView.text_view.setOnTouchListener({ _, _ ->
                hideSoftKeyboard(context as MainActivity)
                false
            })
        }

        fun hideSoftKeyboard(activity: Activity) {
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus?.windowToken, 0)
        }
    }
}